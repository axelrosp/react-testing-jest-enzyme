import React from "react";
import { render } from "@testing-library/react";
import { shallow, configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import App, { Todo } from "./App";

configure({ adapter: new Adapter() });

describe("App", () => {
	describe("Todo", () => {
		test("ejecuta completeTodo cuando pincho Complete", () => {
			//Construimos el mock de la funcion completeTodo (jest.fn() genera un mock)
			const completeTodo = jest.fn();
			const removeTodo = jest.fn();
			const index = 5;
			const todo = {
				isCompleted: true,
				text: "lala",
			};
			//Renderizamos el componente para montar el DOM
			const wrapper = shallow(<Todo todo={todo} index={index} completeTodo={completeTodo} removeTodo={removeTodo} />);
			wrapper.find("#btn_complete_todo").simulate("click");
			//mock.calls nos retorna la cantidad de veces que se ha llamado ese mock
			//a traves de array of arrays, dentro de cada array hay los parametros de cada call
			expect(completeTodo.mock.calls).toEqual([[5]]);
			expect(removeTodo.mock.calls).toEqual([]);
		});

		test("ejecuta removeTodo cuando pincho X", () => {
			//Construimos el mock de la funcion completeTodo (jest.fn() genera un mock)
			const completeTodo = jest.fn();
			const removeTodo = jest.fn();
			const index = 5;
			const todo = {
				isCompleted: true,
				text: "lala",
			};
			const wrapper = shallow(<Todo todo={todo} index={index} completeTodo={completeTodo} removeTodo={removeTodo} />);
			wrapper.find("#btn_remove_todo").simulate("click");
			//mock.calls nos retorna la cantidad de veces que se ha llamado ese mock
			//a traves de array of arrays, dentro de cada array hay los parametros de cada call
			expect(removeTodo.mock.calls).toEqual([[5]]);
			expect(completeTodo.mock.calls).toEqual([]);
		});
	});
});
